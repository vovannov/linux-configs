" general settings
set number relativenumber
set ruler
set encoding=utf-8
set syntax=on
set laststatus=2
set autoindent
set showmode
set showcmd
set ffs=unix,dos
set bg=dark
set wrap
set linebreak
set scrolloff=3

filetype indent on

" sp and vsp setting
set splitbelow splitright

" autocompletion
set wildmode=longest,list,full

" enable mouse in all modes
set mouse=a

" highlight search results
set hlsearch
set incsearch
" ignore case search
set ignorecase

" Tab = 4 spaces
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set smartindent

" show matching brackets
set showmatch

" guicursor breaks terminal cursor
set guicursor=

" split screen navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" autoclose brackets
imap ( ()<Left>
imap [ []<Left>
imap { {}<Left>

" use system clipboard. Leader = '\'
vmap <Leader>y :!xclip -f -sel clip<CR>

" All installed plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'donRaphaco/neotex', { 'for' : 'tex' }
Plug 'nvie/vim-flake8'
Plug 'lervag/vimtex'
Plug 'ntpeters/vim-better-whitespace'
call plug#end()

let g:neotex_pdflatex_alternative = 'xelatex'
let g:tex_flavor = 'latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'

" NERDTree plugin settings
let NERDTreeShowHidden=1
"map <C-o> :NERDTreeToggle<CR>

" some cyrillic key mappings
nnoremap р h
nnoremap о j
nnoremap л k
nnoremap д l
nnoremap н y
nnoremap г u
nnoremap ш i
nnoremap щ o
nnoremap з p
nnoremap ц w
nnoremap п g
nnoremap П G
nnoremap к r
nnoremap с c
nnoremap ф a
nnoremap Ф A
nnoremap м v
nnoremap у e
nnoremap и b
nnoremap ж :
nnoremap Ё ~

"autocmd BufRead,BufNewFile *.tex :NeoTex
"autocmd VimLeave *.tex :NeoTex
"autocmd BufRead,BufNewFile *.tex :NeoTexOn

function egrep --description 'alias egrep grep -Ei --color=auto'
	grep -Ei --color=auto $argv;
end

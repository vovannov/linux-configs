
function ll --description 'alias ll ls -alFhH --group-directories-first'
	ls -alFhH --group-directories-first $argv;
end

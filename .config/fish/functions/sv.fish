function sv --description 'alias sv TERM=xterm-256color sudo vim'
	env TERM=xterm-256color sudo vim $argv;
end

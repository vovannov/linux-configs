#!/usr/bin/env python3

import os
import string

alias_string = string.Template('''
function $func --description 'alias $func $value'
	$value $$argv;
end
''')

aliases = []

with open('aliases.fish', 'r') as f:
    aliases = f.readlines()

aliases_dict = {}

for line in aliases:
    line = line.replace('alias ', '')
    spl_line = line.split()
    aliases_dict[spl_line[0]] = ' '.join(spl_line[1:])
print(aliases_dict)

for key in aliases_dict:
    with open(key + '.fish', 'w') as f:
        f.write(alias_string.substitute({'func': key, 'value': aliases_dict[key]}))
    print(key + ' done')

set fish_greeting 
set -gx PATH /home/vovannov/bin /bin $PATH

# Disable crtl+s and crtl+q
stty -ixon

set -x LESS "-I"
set -x LESS_TERMCAP_mb (printf "\033[01;32m") # green 
set -x LESS_TERMCAP_md (printf "\033[01;32m")
set -x LESS_TERMCAP_so (printf "\033[07;1;38m") # search, black text on white bg
set -x LESS_TERMCAP_us (printf "\033[01;4;31m") # red underlined
set -x LESS_TERMCAP_me (printf "\033[0m")
set -x LESS_TERMCAP_se (printf "\033[0m")
set -x LESS_TERMCAP_ue (printf "\033[0m")

set EDITOR "vim"
set PAGER "less"
set TERM "xterm-256color"

# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob nomatch autopushd pushdignoredups histverify GLOB_DOTS HIST_IGNORE_SPACE HIST_IGNORE_ALL_DUPS HIST_REDUCE_BLANKS
set -C
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/vovannov/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

setopt PROMPT_SUBST

bindkey "\e[H" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[1;5D" backward-word
bindkey "\e[1;5C" forward-word
bindkey "\e[3~" delete-char

set completion-ignore-case
set horisontal-scroll-mode

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

function last_comm_code()
{
    _status="$?"

    red="%{\e[1;31m%}"
    green="%{\e[1;32m%}"
    white="%{\e[0m%}"

    if [ "$_status" = "0" ]; then
        printf "%b%b%b" "$green" "$_status" "$white"
    else
        printf "%b%b%b" "$red" "$_status" "$white"
    fi
}

# Disable crtl+s and crtl+q
stty -ixon

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe.sh ] && eval "$(SHELL=/bin/sh lesspipe.sh)"

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

export LESS='-I'
export LESS_TERMCAP_mb=$'\e[1;32m' # green
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_so=$'\e[7;1;38m' # search, black text on white bg
export LESS_TERMCAP_us=$'\e[1;4;31m' # red underlined
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'

export LS_COLORS="*.png=35:*.jpg=35:*.mp3=35:*.mkv=35:*.avi=35:*.mp4=35:*.7z=01;31:*.zip=01;31:*.tar.gz=01;31:*.tar.xz=01;31:*.tar=01;31"

if [[ -n "$SSH_CLIENT" || -n "$SSH2_CLIENT" ]]; then
    SSH_IN_PROMPT="(ssh)"
else
    SSH_IN_PROMPT=""
fi

PS1=$'%{\e[1;32m%}%n %{\e[1;34m%}%~%{\e[0m%} ($(last_comm_code)) > '

EDITOR="vim"
PAGER="less -r"
TERM="st-256color"

QT_SCALE_FACTOR=1
#QT_QPA_PLATFORMTHEME=gtk2
QT_QPA_PLATFORMTHEME=qt5ct

ZSH_AUTOSUGGEST_USE_ASYNC="1"

# for xterm
XENVIRONMENT="${HOME}/.Xresources"

export EDITOR PAGER PS1 XENVIRONMENT TERM QT_SCALE_FACTOR QT_QPA_PLATFORMTHEME ZSH_AUTOSUGGEST_USE_ASYNC
export PATH=/home/vovannov/.local/bin:/home/vovannov/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

ps -e | grep Xorg > /dev/null || startx

alias grep='grep -Ei --color=auto'
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -alFhH --group-directories-first'
alias l='ls -CFh'
alias q='exit'
alias r='ranger'
alias myip="curl curlmyip.net"
alias clip="xclip -selection clipboard"
alias netfolder="python3 -m http.server"
alias wake="echo $'\a'"
alias mkdir="mkdir -pv"
alias sv="TERM=xterm-256color sudo vim"
alias sp="sudo pacman"
alias si="sudo -i"
alias ssy="sudo systemctl"
alias vi3="vim ~/.config/i3/config"
alias open="xdg-open"
alias m="(pgrep mpd >/dev/null || mpd) && ncmpcpp"
alias update="sudo pacman -Syu"

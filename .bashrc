# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

function last_comm_code()
{
    status="$?"

    red="\e[1;31m"
    green="\e[1;32m"
    white="\e[0m"

    if [ "$status" == "0" ]; then
        printf "%b%b%b" "$green" "$status" "$white"
    else
        printf "%b%b%b" "$red" "$status" "$white"
    fi
}

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Disable crtl+s and crtl+q
[[ $- == *i* ]] && stty -ixon

# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# expand variables by pressing <TAB>
shopt -s direxpand

# better cd
shopt -s cdspell

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

if [ -x /usr/bin/dircolors ]; then
test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" ||
eval "$(dircolors -b)"
fi


# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

export LESS='-I'
export LESS_TERMCAP_mb=$'\e[1;32m' # green 
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_so=$'\e[7;1;38m' # search, black text on white bg
export LESS_TERMCAP_us=$'\e[1;4;31m' # red underlined
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'

if [[ $UID == 0 ]]; then
    USER_COLOR="1" # red color prompt for root user
else
    USER_COLOR="2" # green color prompt for normal user
fi

if [[ -n "$SSH_CLIENT" || -n "$SSH2_CLIENT" ]]; then
    SSH_IN_PROMPT="(ssh)"
else
    SSH_IN_PROMPT=""
fi

PS1='${SSH_IN_PROMPT}\[\e[1;3${USER_COLOR}m\]\u \[\e[1;34m\]\w\[\e[0m\] (\[`last_comm_code`\]) > '

EDITOR="vim"
PAGER="less"
TERM="st-256color"

# for xterm
XENVIRONMENT="${HOME}/.Xresources"

# i-beam cursor in xterm
#[ $TERM = "linux" ] && [ ! -z $DISPLAY ] && echo -e -n "\x1b[\x35 q"

export QT_SCALE_FACTOR=1
export EDITOR PAGER PS1 XENVIRONMENT TERM
export PATH=/home/vovannov/.local/bin:/home/vovannov/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
